### unzipTo

A simple tool to decompress archives to network destinations

At the moment, only 7zip to FTP is supported.

## TODO

Progress and verbosity options
Support pkzip64
Support tar
Support gzip
Support lzma
Support RAR
Support ACE
Support pipes

## Building the application

You will need maven 3

mvn clean
mvn package

That will compile, run tests, and package the application in a single jar.

{project.home}/target/unzipTo-<ver>-jar-with-dependencies.jar

## Usage

java -jar unzipTo.jar <file.7z> <ftp://user:pass@host/path/to/unzip/to>
