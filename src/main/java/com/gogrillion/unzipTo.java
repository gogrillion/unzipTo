package com.gogrillion;

import org.apache.commons.cli.*;
import org.apache.commons.compress.archivers.sevenz.SevenZArchiveEntry;

import java.io.IOException;
import java.util.zip.ZipEntry;

/**
 * Created by grillion on 4/25/15.
 * unzipTo application entry
 */
public class unzipTo {

    public static int main( String[] args ){


        Option oSrcFile = new Option("i", "Source compressed file");
        Option oDestPath = new Option("o", "Destination address.");
        Option help =   new Option("h", "Prints this message");
        Option verbose = new Option("v", "Verbose output");

        Options options = new Options();
        options.addOption(oSrcFile);
        options.addOption(oDestPath);
        options.addOption(help);
        options.addOption(verbose);

        CommandLineParser parser = new GnuParser();
        try {
            CommandLine params = parser.parse(options, args);

            //Print help?
            if( params.hasOption( "h" ) ){
                HelpFormatter hFormat = new HelpFormatter();
                hFormat.printHelp("unzipTo -i srcFile -o destination [-v]", options );
                return 0;
            }

            String srcFile = params.getOptionValue('i');
            String destAddr = params.getOptionValue('o');

            zip7Input zInput;
            ftpDestination dOutput;

            try {
                zInput = new zip7Input(srcFile);
                dOutput = new ftpDestination(destAddr);

                SevenZArchiveEntry zip7;
                while(null != ( zip7 = zInput.getNextFile())){

                }

                zInput.close();
                dOutput.close();

            } catch (IOException ioe ){
                ioe.printStackTrace();
            }

        }
        catch ( ParseException pe ){
            HelpFormatter hFormat = new HelpFormatter();
            hFormat.printHelp("unzipTo -i srcFile -o destination [-v]", options );
            return 1;
        }

        //exit successfully
        return 0;
    }

}
