package com.gogrillion;

import org.apache.commons.compress.archivers.sevenz.SevenZArchiveEntry;
import org.apache.commons.compress.archivers.sevenz.SevenZFile;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Created by Mike Grill on 4/25/15.
 *
 * 7zip source
 */
public class zip7Input {

    private SevenZFile inputZip;
    private SevenZArchiveEntry currentEntry;

    public zip7Input(String filename) throws IOException {
        Path inputPath = Paths.get(filename);
        inputZip = new SevenZFile( inputPath.toFile() );
    }

    public SevenZArchiveEntry getNextFile(){
        try {
            currentEntry = inputZip.getNextEntry();
            if( currentEntry == null ) {
                return null;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return currentEntry;
    }

    public BufferedInputStream getInputStream(  ){
        return null;
    }

    public void close(){
        try {
            inputZip.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
